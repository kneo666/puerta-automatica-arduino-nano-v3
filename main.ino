#include <TimerOne.h>
#include <EEPROM.h>



#define PinCerradura1 13 //L298
#define PinCerradura2 12 //L298
#define PinMotor1 5 // PWM
#define PinMotor2 6 // PWM
#define PinMotor3 10
#define PinMotor4 11
#define PinEncoder 2
#define PinPulso 3

/////////////////////////////////////////////
/////////////////////////////////////////////
volatile int Estado = 0;  //0 = Cerrado 1 = Abriendo  2 = Abierto  3 = Cerrando
volatile int ValorEncoder = 0; // valor del encoder en todo momento
volatile int ValorFis = 0; //conteo del encoder recaudado en el fis
volatile int ValorFreno = 0;
volatile boolean EstadoFis = 0;
volatile int ValorPWM = 150;
volatile int segundos = 0;
volatile int rpm = 0;
volatile int pulsosRPM = 0;
volatile int resolucion = 24;
/////////////////////////////////////////////
/////////////////////////////////////////////


/////////////////////////////////////////////
/////////////////////////////////////////////


void setup() {

  Serial.begin(9600);
  TCCR0B = TCCR0B & B11111000 | B00000001; //tomer0 modificado- multiplicar por 64

  Timer1.initialize(32000000); //0.5 segundos (500.000 * 64)
  Timer1.attachInterrupt(segundero);

  pinMode(PinCerradura1, OUTPUT); //Cerradura + l298
  pinMode(PinCerradura2, OUTPUT); //Cerradura - l298
  pinMode(PinMotor4, OUTPUT);  //Motor4
  pinMode(PinMotor3, OUTPUT);  //Motor3
  pinMode(PinMotor2, OUTPUT);  //Motor2
  pinMode(PinMotor1, OUTPUT);  //Motor1
  pinMode(PinPulso, INPUT_PULLUP); //Pulsos
  pinMode(PinEncoder, INPUT_PULLUP); //Encoder

  attachInterrupt(digitalPinToInterrupt(PinEncoder), encoder, RISING);
  attachInterrupt(digitalPinToInterrupt(PinPulso), pulso, RISING);

  if (EEPROM.read(0) == 1) {
    EstadoFis = 1;
    EEPROM.get(1, ValorFis);

    ValorEncoder = ValorFis;
    ValorFreno = ValorEncoder - (ValorEncoder / 10);
    ValorEncoder = 0;
  }


}

void loop() {

  while (EstadoFis == 0) {
    Fis();
  }


  //LOOP//////////////////////////////////////

  switch (Estado) {
    case 0:       //cerrada



      break;

    case 1:       //abriendo
      abrir();
      calcularPWM();

      break;

    case 2:       //abierta
      parar();


      break;

    case 3:       //cerrando
      cerrar();
      calcularPWM();


      break;

    default:
      error(1);
      parar();
      break;
  }






  //LOOP//////////////////////////////////////

}






///////////////////////////////////////////////////////
///////////////////FUNCIONES///////////////////////////
///////////////////////////////////////////////////////

void encoder() {
  ValorEncoder++;
  pulsosRPM++;



}
void segundero() {
  segundos++;

  rpm = ((double)pulsosRPM / resolucion) * 120; // formula para calcular rpm
  pulsosRPM = 0; // reinicia la cuenta de los pulsos

  if (rpm <= 0) {
    if (Estado == 1) {
      Estado = 2;
    }
    if (Estado == 3) {
      Estado = 0;
    }
  }
}


void calcularPWM() {
  if (ValorEncoder >= 0 && ValorEncoder <= ValorFreno) {
    ValorPWM = 255;
  }
  if (ValorEncoder >= ValorFreno) {
    ValorPWM = 140;
  }
}



void pulso() {

  //RESET FIS
  double TiempoInicio = millis();
  int TiempoDesfase = 256000; //4000*64
  int TiempoFinal = TiempoInicio + TiempoFinal;
  while (digitalRead(PinPulso) == 0) {
    if (TiempoFinal >= millis()) {
      EEPROM.write(0, 0); //EstadoFis = 0;
      EstadoFis = 0;
    }
  }

  //SUMA E INVIERTE EL GIRO SI ESTA EN MOVIMIENTO
  if (Estado == 1 || Estado == 3) {
    if (ValorEncoder < ValorFis && rpm > 0) {
      parar();
      ValorEncoder = ValorEncoder - ValorFis;
      Estado++;

    }
  }

  //SUMA SOLO SI ESTA DETENIDO
  if (Estado == 0 || Estado == 2) {
    Estado++;
  }



  if (Estado > 3) {
    Estado = 0;
  }

}



void error(int N) {
  if (N == 1) {
    Serial.println("Error 1");
  }
  if (N == 1) {
    Serial.println("Error 2");
  }
  if (N == 1) {
    Serial.println("Error 3");
  }
  if (N == 1) {
    Serial.println("Error 4");
  }
}



void Fis() {

  abrir();
  delay(100);

  ValorEncoder;
  ValorFis;
  ValorFreno;

  parar();
  cerrar();
  while (rpm != 0.0) {
  }
  parar();


  ValorFis = ValorEncoder;
  ValorFreno = ValorEncoder - (ValorEncoder / 10);
  ValorEncoder = 0;

  EEPROM.write(0, 1); //Guarda el estado del fis
  EEPROM.update(1, ValorFis); //guarda el valor del dis en la eeprom

  EstadoFis = 1;  //Realizado
}


void parar() {
  digitalWrite(PinMotor1, HIGH);
  digitalWrite(PinMotor2, HIGH);
  digitalWrite(PinMotor3, LOW);
  digitalWrite(PinMotor4, LOW);
  Serial.println("Parar");
}

void abrir() {
  analogWrite(PinMotor1, ValorPWM);
  digitalWrite(PinMotor2, LOW);
  digitalWrite(PinMotor3, LOW);
  digitalWrite(PinMotor4, HIGH);
  Serial.println("Abrir");

}

void cerrar() {
  digitalWrite(PinMotor1, LOW);
  analogWrite(PinMotor2, ValorPWM);
  digitalWrite(PinMotor3, HIGH);
  digitalWrite(PinMotor4, LOW);
  Serial.println("Cerrar");

}


///////////////////////////////////////////////////////
///////////////////FUNCIONES///////////////////////////
///////////////////////////////////////////////////////